import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {of} from 'rxjs';

import {CalendarComponent} from './calendar.component';
import {DateService} from './date.service';
import {TaskListService} from './task-list.service';
import {TaskService} from './task.service';
import {TaskList} from './taskList.model';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;
  let dateService: jasmine.SpyObj<DateService>;
  let taskListService: jasmine.SpyObj<TaskListService>;

  const date1: Date = new Date('2019-06-20T00:00:00.000Z');
  const date2: Date = new Date('2019-06-21T00:00:00.000Z');
  const date3: Date = new Date('2019-06-22T00:00:00.000Z');
  const date4: Date = new Date('2019-06-23T00:00:00.000Z');
  const date5: Date = new Date('2019-06-24T00:00:00.000Z');
  const date6: Date = new Date('2019-06-25T00:00:00.000Z');
  const date7: Date = new Date('2019-06-26T00:00:00.000Z');

  const taskList1: TaskList = {date: date1, list: []};
  const taskList2: TaskList = {date: date2, list: []};
  const taskList3: TaskList = {date: date3, list: []};
  const taskList4: TaskList = {date: date4, list: []};
  const taskList5: TaskList = {date: date5, list: []};
  const taskList6: TaskList = {date: date6, list: []};
  const taskList7: TaskList = {date: date7, list: []};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }, {
        provide: DateService,
        useValue: jasmine.createSpyObj('dateService', ['getInitialDates', 'getPreviousDates', 'getNextDates', 'getDatePageForStartDate'])
      }, {
        provide: TaskListService,
        useValue: jasmine.createSpyObj('taskListService', ['getTaskListsByPriorityDates'])
      }, {
        provide: TaskService,
        useValue: jasmine.createSpyObj('taskService', ['createNewTask', 'isTaskDueToday', 'isTaskDueTomorrow'])
      }
      ]
    }).overrideTemplate(CalendarComponent, '')
      .compileComponents();

    dateService = TestBed.get(DateService);
    taskListService = TestBed.get(TaskListService);
  }));

  beforeEach(() => {
    dateService.getInitialDates.and.returnValue(of([
      date1, date2, date3, date4, date5, date6, date7
    ]));
    taskListService.getTaskListsByPriorityDates.and.returnValue(of([
      taskList1, taskList2, taskList3, taskList4, taskList5, taskList6, taskList7
    ]));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get initial dates', () => {
    // when --> onInit

    // then
    expect(dateService.getInitialDates).toHaveBeenCalled();
  });

  it('should set dates', () => {
    // when --> onInit

    // then
    expect(component.startDate).toBe(date1);
    expect(component.startDatePlusOne).toBe(date2);
    expect(component.startDatePlusTwo).toBe(date3);
    expect(component.startDatePlusThree).toBe(date4);
    expect(component.startDatePlusFour).toBe(date5);
    expect(component.startDatePlusFive).toBe(date6);
    expect(component.startDatePlusSix).toBe(date7);
  });

  it('should set taskLists', () => {
    // when --> onInit

    // then
    expect(component.taskList1).toBe(taskList1);
    expect(component.taskList2).toBe(taskList2);
    expect(component.taskList3).toBe(taskList3);
    expect(component.taskList4).toBe(taskList4);
    expect(component.taskList5).toBe(taskList5);
    expect(component.taskList6).toBe(taskList6);
    expect(component.taskList7).toBe(taskList7);
  });

  it('should set isDateRangeInSameMonth', () => {
    // when --> onInit

    // then
    expect(component.isDateRangeInSameMonth).toBeTruthy();
  });

  it('should set isDateRangeInSameMonth', () => {
    // when --> onInit

    // then
    expect(component.isStartDateToday).toBeTruthy();
  });

  it('should get previous dates', () => {
    // given
    dateService.getNextDates.and.returnValue(of([
      new Date('2019-06-13T00:00:00.000Z'),
      new Date('2019-06-14T00:00:00.000Z'),
      new Date('2019-06-15T00:00:00.000Z'),
      new Date('2019-06-16T00:00:00.000Z'),
      new Date('2019-06-17T00:00:00.000Z'),
      new Date('2019-06-18T00:00:00.000Z'),
      new Date('2019-06-19T00:00:00.000Z'),
    ]));

    taskListService.getTaskListsByPriorityDates.and.returnValue(of([]));

    // when
    component.previousDatePage();

    // then
    expect(dateService.getNextDates).toHaveBeenCalledWith(new Date('2019-06-13T00:00:00.000Z'));
    expect(component.isStartDateToday).toBe(false);
    expect(component.isDateRangeInSameMonth).toBe(true);
  });

  it('should get next dates', () => {
    // given
    dateService.getNextDates.and.returnValue(of([
      new Date('2019-06-27T00:00:00.000Z'),
      new Date('2019-06-28T00:00:00.000Z'),
      new Date('2019-06-29T00:00:00.000Z'),
      new Date('2019-06-30T00:00:00.000Z'),
      new Date('2019-07-01T00:00:00.000Z'),
      new Date('2019-07-02T00:00:00.000Z'),
      new Date('2019-07-03T00:00:00.000Z'),
    ]));

    taskListService.getTaskListsByPriorityDates.and.returnValue(of([]));

    // when
    component.nextDatePage();

    // then
    expect(dateService.getNextDates).toHaveBeenCalledWith(new Date('2019-06-27T00:00:00.000Z'));
    expect(component.isStartDateToday).toBe(false);
    expect(component.isDateRangeInSameMonth).toBe(false);
  });

  it('should get specific dates', () => {
    // given
    const specificDate = new Date('2019-01-01T00:00:00.000Z');
    dateService.getNextDates.and.returnValue(of([
      specificDate,
      new Date('2019-01-02T00:00:00.000Z'),
      new Date('2019-01-03T00:00:00.000Z'),
      new Date('2019-01-04T00:00:00.000Z'),
      new Date('2019-01-05T00:00:00.000Z'),
      new Date('2019-01-06T00:00:00.000Z'),
      new Date('2019-01-07T00:00:00.000Z'),
    ]));

    taskListService.getTaskListsByPriorityDates.and.returnValue(of([]));

    // when
    component.specificStartDatePage(specificDate);

    // then
    expect(dateService.getNextDates).toHaveBeenCalledWith(specificDate);
    expect(component.isStartDateToday).toBe(false);
    expect(component.isDateRangeInSameMonth).toBe(true);
  });
});
