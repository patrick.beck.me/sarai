import {Task} from './task.model';

export interface TaskList {
  date: Date;
  list: Task[];
}
