import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {User} from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private firebaseConfig = {
    apiKey: 'AIzaSyDakXpOReFEOGrV6JVgkffqF5MSve_f3uU',
    authDomain: 'sarai-be227.firebaseapp.com',
    databaseURL: 'https://sarai-be227.firebaseio.com',
    projectId: 'sarai-be227',
    storageBucket: 'sarai-be227.appspot.com',
    messagingSenderId: '763342214459',
    appId: '1:763342214459:web:481f160c2f87f46c'
  };

  private provider: any = new firebase.auth.GoogleAuthProvider().addScope('https://www.googleapis.com/auth/contacts.readonly');

  private currentUser: User;

  constructor() {
    firebase.initializeApp(this.firebaseConfig);
  }

  login(): void {
    // TODO: CLEAN UP
    firebase.auth().signInWithPopup(this.provider).then((result) => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      let token = result.credential;
      this.currentUser = {
        id: result.user.uid,
        displayName: result.user.displayName,
        eMail: result.user.email,
        avatarUrl: result.user.photoURL
      }
    }).catch((error) => {
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
      // The email of the user's account used.
      let email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      let credential = error.credential;
      // ...
    });
  }

  logout(): void {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      this.currentUser = undefined;
    }).catch(error => {
      // An error happened.
    });
  }

  isLoggedIn(): boolean {
    return this.currentUser !== undefined;
  }

  getCurrentUser(): User {
    return this.currentUser;
  }

}
