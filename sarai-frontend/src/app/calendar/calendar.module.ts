import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule, MatInputModule,
  MatToolbarModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule, MatRippleModule
} from '@angular/material';
import {AddNewTaskDialogComponent} from './calendar-card/add-new-task-dialog/add-new-task-dialog.component';
import {CalendarCardComponent} from './calendar-card/calendar-card.component';
import {CalendarComponent} from './calendar.component';
import {TaskDetailsDialogComponent} from './calendar-card/task-details-dialog/task-details-dialog.component';

@NgModule({
  declarations: [
    CalendarComponent,
    CalendarCardComponent,
    AddNewTaskDialogComponent,
    TaskDetailsDialogComponent
  ],
  imports: [
    CommonModule,
    MatGridListModule,
    MatDividerModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    DragDropModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatRippleModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
  ],
  entryComponents: [
    AddNewTaskDialogComponent,
    TaskDetailsDialogComponent
  ]
})
export class CalendarModule {
}
