import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';

import {TaskListService} from './task-list.service';

describe('TaskListService', () => {
  let taskListService: TaskListService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    taskListService = TestBed.get(TaskListService);
  });

  it('should be created', () => {
    expect(taskListService).toBeTruthy();
  });
});
