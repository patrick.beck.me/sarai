import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {Observable, of} from 'rxjs';
import {Task} from '../task.model';
import {TaskService} from '../task.service';

import {CalendarCardComponent} from './calendar-card.component';

describe('CalendarCardComponent', () => {
  let component: CalendarCardComponent;
  let fixture: ComponentFixture<CalendarCardComponent>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let taskService: jasmine.SpyObj<TaskService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarCardComponent],
      providers: [CalendarCardComponent, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }, {
        provide: TaskService,
        useValue: jasmine.createSpyObj('taskService', ['createNewTask', 'isTaskDueToday', 'isTaskDueTomorrow'])
      }]
    }).overrideTemplate(CalendarCardComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    taskService = TestBed.get(TaskService);

    fixture = TestBed.createComponent(CalendarCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.date = new Date();
    component.taskList = [];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open dialog', () => {
    // given
    const matDialogRef = {
      afterClosed(): Observable<any> {
        return of(undefined);
      }
    };
    dialog.open.and.returnValue(matDialogRef);

    // when
    component.openAddTaskDialog();

    // then
    expect(dialog.open).toHaveBeenCalled();
  });

  it('should push task to taskList and set due flags', () => {
    // given
    const title = 'title';
    const task: Task = {
      id: undefined,
      title: title,
      project: '1',
      priorityDate: new Date(),
      isDueToday: false,
      isDueTomorrow: false
    };

    const matDialogRef = {
      afterClosed() {
      }
    };
    spyOn(matDialogRef, 'afterClosed').and.returnValue(of(task));
    dialog.open.and.returnValue(matDialogRef);
    taskService.isTaskDueToday.and.returnValue(of(true));
    taskService.isTaskDueTomorrow.and.returnValue(of(false));
    taskService.createNewTask.and.returnValue(of('task-id'));


    // when
    component.openAddTaskDialog();

    // then
    expect(matDialogRef.afterClosed).toHaveBeenCalled();
    expect(taskService.isTaskDueToday).toHaveBeenCalled();
    expect(taskService.isTaskDueTomorrow).toHaveBeenCalled();
    expect(component.taskList.length).toBe(1);
    expect(component.taskList[0].title).toBe(title);
    expect(component.taskList[0].isDueTomorrow).toBeFalsy();
    expect(component.taskList[0].isDueToday).toBeTruthy();
  });

  it('should create new task by using the task service', () => {
    // given
    const task: Task = {
      id: undefined,
      title: 'title',
      project: '1',
      priorityDate: new Date(),
      isDueToday: false,
      isDueTomorrow: false
    };

    const matDialogRef = {
      afterClosed() {
      }
    };
    spyOn(matDialogRef, 'afterClosed').and.returnValue(of(task));
    dialog.open.and.returnValue(matDialogRef);
    taskService.isTaskDueToday.and.returnValue(of(true));
    taskService.isTaskDueTomorrow.and.returnValue(of(false));
    taskService.createNewTask.and.returnValue(of('task-id'));


    // when
    component.openAddTaskDialog();

    // then
    expect(taskService.createNewTask).toHaveBeenCalled();
  });
});
