import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {AuthenticationService} from '../authentication/authentication.service';
import {DateService} from './date.service';
import {TaskHttpResource} from './task-http-resource.model';
import {Task} from './task.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private dateService: DateService,
              private authService: AuthenticationService,
              private http: HttpClient) {
  }

  createNewTask(newTask: Task): Observable<any> {
    const body: TaskHttpResource = {
      id: newTask.id,
      userId: this.authService.getCurrentUser().id,
      title: newTask.title,
      description: newTask.description,
      dueDate: newTask.dueDate ? newTask.dueDate.toISOString() : '',
      priorityDate: newTask.priorityDate.toISOString(),
      project: newTask.project
    };
    return this.http.post(environment.backendUrl + 'task/create', body);
  }

  updateTask(updatedTask: Task): void {
    const body: TaskHttpResource = {
      id: updatedTask.id,
      userId: this.authService.getCurrentUser().id,
      title: updatedTask.title,
      description: updatedTask.description,
      dueDate: updatedTask.dueDate ? updatedTask.dueDate.toISOString() : '',
      priorityDate: updatedTask.priorityDate.toISOString(),
      project: updatedTask.project
    };
    this.http.put(environment.backendUrl + 'task/update', body).subscribe();
  }

  deleteTask(taskId: string): void {
    this.http.delete(environment.backendUrl + 'task/delete', {params: {id: taskId}}).subscribe();
  }

  isTaskDueToday(task: Task): Observable<boolean> {
    return of(task.dueDate ? this.dateService.isDateToday(task.dueDate) : false);
  }

  isTaskDueTomorrow(task: Task): Observable<boolean> {
    return of(task.dueDate ? this.dateService.isDateTomorrow(task.dueDate) : false);
  }
}
