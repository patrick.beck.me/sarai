import {AbstractControl, ValidatorFn} from '@angular/forms';
import * as moment from 'moment';

export function dueDateValidator(givenDate: Date): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (givenDate === undefined) {
      return null;
    }
    const forbidden = moment(control.value).isBefore(givenDate, 'day');
    return forbidden ? {'forbiddenDate': {value: control.value}} : null;
  };
}
