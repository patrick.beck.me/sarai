import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {zip} from 'rxjs';
import {Task} from '../task.model';
import {TaskService} from '../task.service';
import {AddNewTaskDialogComponent} from './add-new-task-dialog/add-new-task-dialog.component';
import * as uuid from 'uuid';

@Component({
  selector: 'app-calendar-card',
  templateUrl: './calendar-card.component.html',
  styleUrls: ['./calendar-card.component.css']
})
export class CalendarCardComponent {

  @Input() date: Date;

  @Input() taskList: Task[];

  constructor(
    public dialog: MatDialog,
    private taskService: TaskService) {
  }

  openAddTaskDialog(): void {
    const dialogRef = this.dialog.open(AddNewTaskDialogComponent, {
      width: '548px',
      data: {
        task: {
          id: uuid.v4(),
          title: '',
          description: '',
          dueDate: undefined,
          project: '',
          priorityDate: this.date,
          isDueToday: false,
          isDueTomorrow: false
        }
      }
    });

    dialogRef.afterClosed().subscribe((task: Task) => {
      if (task) {
        zip(
          this.taskService.isTaskDueToday(task),
          this.taskService.isTaskDueTomorrow(task)
        ).subscribe((results) => {
          task.isDueToday = results[0];
          task.isDueTomorrow = results[1];
          this.taskService.createNewTask(task).subscribe((response) => {
            task.id = response.id;
            this.taskList.push(task);
          });
        });
      }
    });
  }
}
