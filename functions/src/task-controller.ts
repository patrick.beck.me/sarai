import * as uuid from 'uuid';
import * as express from 'express';
import * as cors from 'cors';
import * as _ from 'lodash';

export default function (db) {
    const taskController = express();
    taskController.use(cors({origin: true}));

    taskController.post('/create', async (req, res) => {
        const taskId = uuid.v4();
        const newTask = await createTaskObject(req.body, taskId);
        db.collection('tasks').doc(taskId).set(newTask)
            .then(() => {
                res.status(201).send({id: taskId});
            })
            .catch(err => {
                res.status(500).send(err);
            });
    });

    function createTaskObject(requestBody: any, taskId: string): Promise<any> {
        if (!requestBody || _.isEmpty(requestBody)) {
            return Promise.reject('Empty request body!');
        }
        return Promise.resolve({
            id: taskId,
            userId: requestBody.userId,
            title: requestBody.title,
            description: requestBody.description ? requestBody.description : '',
            dueDate: requestBody.dueDate ? requestBody.dueDate : '',
            priorityDate: requestBody.priorityDate,
            project: requestBody.project
        });
    }

    taskController.get('/list', async (req, res) => {
        db.collection('tasks')
            .where('priorityDate', '>=', req.query.startDate)
            .where('priorityDate', '<=', req.query.endDate)
            .where('userId', '==', req.query.userId)
            .get()
            .then(snapshot => {
                if (snapshot.empty) {
                    console.log('No matching documents.');
                    res.status(200).send({});
                    return;
                }

                let tasks = [];
                snapshot.forEach(doc => {
                    tasks.push(doc.data());
                    console.log(doc.id, '=>', doc.data());
                });
                res.status(200).send(tasks);
            })
            .catch(err => {
                console.log('Error getting documents', err);
                res.status(500).send(err);
            });
    });

    taskController.put('/update', async (req, res) => {
        if (!req.body.id) {
            res.status(500).send("No task ID provided!");
        }
        const updatedTask = await createTaskObject(req.body, req.body.id);
        const docRef = db.collection('tasks').doc(req.body.id);
        docRef.get().then((doc) => {
            if (doc) {
                docRef.update(updatedTask);
            } else {
                docRef.set(updatedTask);
            }
        }).finally(() => {
            res.status(204).send();
        }).catch((error) => {
            res.status(500).send(error);
        });
    });

    taskController.delete('/delete', async (req, res) => {
        if (!req.query.id) {
            res.status(403).send("No task ID provided!");
        }
        const docRef = await db.collection('tasks').doc(req.query.id);
        docRef.get().then((doc) => {
            if (doc.exists) {
                docRef.delete().then(() => {
                    res.status(200).send('Deleted task with ID: ' + req.query.id);
                }).catch((error) => {
                    res.status(500).send(error);
                });
            } else {
                res.status(404).send('No task found with ID: ' + req.query.id);
            }
        }).catch((error) => {
            res.status(500).send(error);
        });
    });

    return taskController;
}