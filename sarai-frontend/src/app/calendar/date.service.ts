import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  private today: Date;

  constructor() {
    DateService.correctDateTime(new Date()).subscribe((date) => {
      this.today = date;
    });
  }

  static correctDateTime(date: Date): Observable<Date> {
    const correctedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    return of(correctedDate);
  }

  static getDates(initialDate: Date): Date[] {
    const startDate = initialDate;
    const startDatePlusOne = new Date(startDate);
    startDatePlusOne.setDate(startDate.getDate() + 1);
    const startDatePlusTwo = new Date(startDate);
    startDatePlusTwo.setDate(startDate.getDate() + 2);
    const startDatePlusThree = new Date(startDate);
    startDatePlusThree.setDate(startDate.getDate() + 3);
    const startDatePlusFour = new Date(startDate);
    startDatePlusFour.setDate(startDate.getDate() + 4);
    const startDatePlusFive = new Date(startDate);
    startDatePlusFive.setDate(startDate.getDate() + 5);
    const startDatePlusSix = new Date(startDate);
    startDatePlusSix.setDate(startDate.getDate() + 6);
    return [startDate, startDatePlusOne, startDatePlusTwo, startDatePlusThree, startDatePlusFour, startDatePlusFive, startDatePlusSix];
  }

  getNextDates(startDate: Date): Observable<Date[]> {
    return DateService.correctDateTime(startDate)
      .pipe(map(correctedDate => {
        return DateService.getDates(correctedDate);
      }));
  }

  getInitialDates(): Observable<Date[]> {
    return of(DateService.getDates(this.today));
  }

  isDateToday(date: Date): boolean {
    return date.getDate() === this.today.getDate();
  }

  isDateTomorrow(date: Date): boolean {
    const tomorrow = new Date();
    tomorrow.setDate(this.today.getDate() + 1);
    return date.getDate() === tomorrow.getDate();
  }
}
