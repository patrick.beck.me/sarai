import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {of} from 'rxjs';
import {DateService} from '../../date.service';
import {DialogData} from '../task-details-dialog/task-details-dialog.component';

import {AddNewTaskDialogComponent} from './add-new-task-dialog.component';

describe('AddNewTaskDialogComponent', () => {
  const priorityDate = new Date('2019-06-24T09:58:50.556Z');
  let component: AddNewTaskDialogComponent;
  let fixture: ComponentFixture<AddNewTaskDialogComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<AddNewTaskDialogComponent>>;
  const data: DialogData = {
    task: {
      id: undefined,
      title: '',
      description: '',
      dueDate: new Date('2019-06-29T09:58:50.556Z'),
      priorityDate: priorityDate,
      isDueTomorrow: false,
      isDueToday: false,
      project: ''
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddNewTaskDialogComponent],
      providers: [AddNewTaskDialogComponent, {
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: data
      }]
    }).overrideTemplate(AddNewTaskDialogComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    dialogRef = TestBed.get(MatDialogRef);

    fixture = TestBed.createComponent(AddNewTaskDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog on cancel', () => {
    // when
    component.onCancel();

    // then
    expect(dialogRef.close).toHaveBeenCalled();
  });

  it('should close dialog with taskForm value', () => {
    // when
    component.onSubmit();

    // then
    expect(dialogRef.close).toHaveBeenCalledWith(component.taskForm.value);
  });

  it('should correct the date time of the due date', () => {
    // given
    const correctedDate = new Date('2019-06-29T00:00:00.000Z');
    spyOn(DateService, 'correctDateTime').and.returnValue(of(correctedDate));
    component.taskForm.value.dueDate = new Date();

    // when
    component.onSubmit();

    // then
    expect(DateService.correctDateTime).toHaveBeenCalled();
    expect(component.taskForm.value.dueDate).toEqual(correctedDate);
  });

  it('should set the priority date ', () => {
    // when
    component.onSubmit();

    // then
    expect(component.taskForm.value.priorityDate).toEqual(priorityDate);
  });
});
