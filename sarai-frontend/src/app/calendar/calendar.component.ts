import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Subscription, zip} from 'rxjs';
import * as moment from 'moment';
import {TaskDetailsDialogComponent} from './calendar-card/task-details-dialog/task-details-dialog.component';
import {DateService} from './date.service';
import {TaskListService} from './task-list.service';
import {Task} from './task.model';
import {TaskService} from './task.service';
import {TaskList} from './taskList.model';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnDestroy {

  private datesSubscription: Subscription;

  startDate: Date;
  startDatePlusOne: Date;
  startDatePlusTwo: Date;
  startDatePlusThree: Date;
  startDatePlusFour: Date;
  startDatePlusFive: Date;
  startDatePlusSix: Date;

  taskList1: TaskList = {date: undefined, list: []};
  taskList2: TaskList = {date: undefined, list: []};
  taskList3: TaskList = {date: undefined, list: []};
  taskList4: TaskList = {date: undefined, list: []};
  taskList5: TaskList = {date: undefined, list: []};
  taskList6: TaskList = {date: undefined, list: []};
  taskList7: TaskList = {date: undefined, list: []};

  isDateRangeInSameMonth: boolean;
  isStartDateToday: boolean;

  constructor(
    public dialog: MatDialog,
    private dateService: DateService,
    private taskListService: TaskListService,
    private taskService: TaskService) {
  }

  ngOnInit() {
    this.initialDatePage();
  }

  initialDatePage(): void {
    this.datesSubscription = this.dateService.getInitialDates().subscribe((dates) => {
      this.setDates(dates);
      this.taskListService.getTaskListsByPriorityDates(dates).subscribe((taskLists) => {
        this.setTaskLists(taskLists);
        this.setDateRangeInSameMonth();
        this.isStartDateToday = true;
      });
    });
  }

  previousDatePage(): void {
    const startDate = new Date(this.startDate);
    startDate.setDate(this.startDate.getDate() - 7);
    this.dateService.getNextDates(startDate).subscribe(previousDates => {
      this.setDates(previousDates);
      this.taskListService.getTaskListsByPriorityDates(previousDates).subscribe((taskLists) => {
        this.setTaskLists(taskLists);
        this.setDateRangeInSameMonth();
        this.isStartDateToday = this.dateService.isDateToday(startDate);
      });
    });
  }

  nextDatePage(): void {
    const startDate = new Date(this.startDatePlusSix);
    startDate.setDate(this.startDatePlusSix.getDate() + 1);
    this.dateService.getNextDates(startDate).subscribe(nextDates => {
      this.setDates(nextDates);
      this.taskListService.getTaskListsByPriorityDates(nextDates).subscribe((taskLists) => {
        this.setTaskLists(taskLists);
        this.setDateRangeInSameMonth();
        this.isStartDateToday = this.dateService.isDateToday(startDate);
      });
    });
  }

  specificStartDatePage(specificDate: Date): void {
    this.dateService.getNextDates(specificDate).subscribe((dates) => {
      this.setDates(dates);
      this.taskListService.getTaskListsByPriorityDates(dates).subscribe((taskLists) => {
        this.setTaskLists(taskLists);
        this.setDateRangeInSameMonth();
        this.isStartDateToday = this.dateService.isDateToday(specificDate);
      });
    });
  }

  drop(event: CdkDragDrop<Task[]>, dropDate: Date): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      const task = event.previousContainer.data[event.previousIndex];
      const prevPriorityDate = task.priorityDate;
      task.priorityDate = dropDate;
      zip(
        this.taskService.isTaskDueToday(task),
        this.taskService.isTaskDueTomorrow(task)
      ).subscribe((results) => {
        task.isDueToday = results[0];
        task.isDueTomorrow = results[1];
        this.taskService.updateTask(task);
      });
      if (!task.dueDate || moment(task.dueDate).isSameOrAfter(task.priorityDate, 'day')) {
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      } else {
        task.priorityDate = prevPriorityDate;
      }
    }
  }

  openTaskDetailsDialog(task: Task, taskList: TaskList) {
    const dialogRef = this.dialog.open(TaskDetailsDialogComponent, {
      data: {
        task: task
      }
    });

    dialogRef.afterClosed().subscribe(updatedTask => {
      if (updatedTask) {
        if (updatedTask.isDeleted) {
          this.optimisticDeleteOfTask(taskList, task);
        } else {
          zip(
            this.taskService.isTaskDueToday(updatedTask),
            this.taskService.isTaskDueTomorrow(updatedTask)
          ).subscribe((results) => {
            updatedTask.isDueToday = results[0];
            updatedTask.isDueTomorrow = results[1];
            task.isDueToday = results[0];
            task.isDueTomorrow = results[1];
            this.updateTask(taskList, updatedTask);
          });
        }
      }
    });
  }

  private optimisticDeleteOfTask(taskList: TaskList, task: Task) {
    const index = taskList.list.findIndex(element => element.id === task.id);
    taskList.list.splice(index, 1);
  }

  private updateTask(taskList: TaskList, task: Task): void {
    this.taskService.updateTask(task);
    this.optimisticUpdateOfTaskList(task, taskList);
  }

  private optimisticUpdateOfTaskList(task: Task, taskList: TaskList) {

    if (task.priorityDate === taskList.date) {
      const index = taskList.list.findIndex(element => element.id === task.id);
      taskList[index] = task;
    } else {
      this.optimisticMoveToTaskList(task, taskList);
    }
  }

  private optimisticMoveToTaskList(updatedTask: Task, previousTaskList: TaskList): void {
    switch (true) {
      case (updatedTask.priorityDate.getDate() === this.taskList1.date.getDate()):
        this.taskList1.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList2.date.getDate()):
        this.taskList2.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList3.date.getDate()):
        this.taskList3.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList4.date.getDate()):
        this.taskList4.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList5.date.getDate()):
        this.taskList5.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList6.date.getDate()):
        this.taskList6.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      case (updatedTask.priorityDate.getDate() === this.taskList7.date.getDate()):
        this.taskList7.list.push(updatedTask);
        this.removeFromTaskList(previousTaskList.list, updatedTask);
        break;
      default:
        this.removeFromTaskList(previousTaskList.list, updatedTask);
    }
  }

  private removeFromTaskList(taskList: Task[], task: Task): void {
    const index = taskList.findIndex(element => element.id === task.id);
    taskList.splice(index, 1);
  }

  private setDates(dates: Date[]): void {
    this.startDate = dates[0];
    this.startDatePlusOne = dates[1];
    this.startDatePlusTwo = dates[2];
    this.startDatePlusThree = dates[3];
    this.startDatePlusFour = dates[4];
    this.startDatePlusFive = dates[5];
    this.startDatePlusSix = dates[6];
  }

  private setTaskLists(taskLists: TaskList[]): void {
    this.taskList1 = taskLists[0];
    this.taskList2 = taskLists[1];
    this.taskList3 = taskLists[2];
    this.taskList4 = taskLists[3];
    this.taskList5 = taskLists[4];
    this.taskList6 = taskLists[5];
    this.taskList7 = taskLists[6];
  }

  private setDateRangeInSameMonth() {
    this.isDateRangeInSameMonth = this.startDate.getMonth() === this.startDatePlusSix.getMonth();
  }

  ngOnDestroy(): void {
    this.datesSubscription.unsubscribe();
  }
}
