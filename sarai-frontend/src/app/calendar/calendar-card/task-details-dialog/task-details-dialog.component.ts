import {AfterViewInit, ChangeDetectorRef, Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {dueDateToPriorityDateValidator} from '../../../shared/validator/validate-due-date-to-priority-date.validator';
import {DateService} from '../../date.service';
import {Task} from '../../task.model';
import {TaskService} from '../../task.service';
import {Project} from '../add-new-task-dialog/add-new-task-dialog.component';

@Component({
  selector: 'app-task-details-dialog',
  templateUrl: './task-details-dialog.component.html',
  styleUrls: ['./task-details-dialog.component.css']
})
export class TaskDetailsDialogComponent implements AfterViewInit {

  taskUpdateForm = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl(''),
    dueDate: new FormControl(''),
    priorityDate: new FormControl('', [Validators.required]),
    project: new FormControl('', Validators.required)
  }, {validators: dueDateToPriorityDateValidator});

  projects: Project[] = [
    {value: '0', viewValue: 'Work'},
    {value: '1', viewValue: 'Daily stuff'},
    {value: '2', viewValue: 'Friends'}
  ];

  constructor(
    public dialogRef: MatDialogRef<TaskDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private cd: ChangeDetectorRef,
    private taskService: TaskService) {
  }

  onCancel(): void {
    this.dialogRef.close();
    this.taskUpdateForm.reset(this.data.task);
  }

  onSubmit(): void {
    DateService.correctDateTime(this.taskUpdateForm.value.priorityDate).subscribe(correctedDate => {
      this.taskUpdateForm.value.id = this.data.task.id;
      this.taskUpdateForm.value.priorityDate = correctedDate;
    });
    if (this.taskUpdateForm.value.dueDate) {
      DateService.correctDateTime(this.taskUpdateForm.value.dueDate).subscribe(correctedDate => {
        this.taskUpdateForm.value.dueDate = correctedDate;
      });
    }
    this.dialogRef.close(this.taskUpdateForm.value);
  }

  onDelete(): void {
    this.taskService.deleteTask(this.data.task.id);
    this.data.task.isDeleted = true;
    this.dialogRef.close(this.data.task);
  }

  getTitleErrorMessage() {
    return 'You must enter a title';
  }

  getPriorityDateErrorMessage() {
    return 'You must select a valid priority date';
  }

  getProjectErrorMessage() {
    return 'You must select a project';
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

}

export interface DialogData {
  task: Task;
}
