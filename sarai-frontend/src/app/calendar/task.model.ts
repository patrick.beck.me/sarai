export interface Task {
  id: string;
  title: string;
  description?: string;
  dueDate?: Date;
  priorityDate: Date;
  project: any;
  isDueToday: boolean;
  isDueTomorrow: boolean;
  isDeleted: boolean;
}
