import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {DateService} from '../calendar/date.service';
import {TaskListService} from '../calendar/task-list.service';
import {TaskService} from '../calendar/task.service';

import {NavigationComponent} from './navigation.component';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavigationComponent],
      providers: [NavigationComponent, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }, {
        provide: TaskService,
        useValue: jasmine.createSpyObj('taskService', ['createNewTask', 'isTaskDueToday', 'isTaskDueTomorrow'])
      }]
    }).overrideTemplate(NavigationComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
