export interface Project {
  name: string;
  color: string;
  description: string;
}
