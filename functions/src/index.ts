import * as functions from 'firebase-functions';
import * as  admin from 'firebase-admin';
import taskController from './task-controller';

admin.initializeApp();
let db = admin.firestore();
db.settings({timestampsInSnapshots: true});

exports.task = functions.region('europe-west1').https.onRequest(taskController(db));