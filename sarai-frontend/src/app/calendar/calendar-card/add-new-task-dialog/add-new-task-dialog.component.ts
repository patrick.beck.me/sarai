import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {dueDateValidator} from '../../../shared/validator/due-date.validator';
import {DateService} from '../../date.service';
import {Task} from '../../task.model';

@Component({
  selector: 'app-add-new-task-dialog',
  templateUrl: './add-new-task-dialog.component.html',
  styleUrls: ['./add-new-task-dialog.component.css']
})
export class AddNewTaskDialogComponent {

  taskForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    description: new FormControl(''),
    dueDate: new FormControl('', dueDateValidator(this.data.task.priorityDate)),
    project: new FormControl('', [Validators.required])
  });

  projects: Project[] = [
    {value: '0', viewValue: 'Work'},
    {value: '1', viewValue: 'Daily stuff'},
    {value: '2', viewValue: 'Friends'}
  ];

  constructor(
    public dialogRef: MatDialogRef<AddNewTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.taskForm.value.dueDate) {
      DateService.correctDateTime(this.taskForm.value.dueDate).subscribe((correctedDate) => {
        this.taskForm.value.dueDate = correctedDate;
      });
    }
    this.taskForm.value.priorityDate = this.data.task.priorityDate;
    this.dialogRef.close(this.taskForm.value);
  }

  getTitleErrorMessage() {
    return 'You must enter a title';
  }

  getProjectErrorMessage() {
    return 'You must select a project';
  }

  getDueDateErrorMessage() {
    return 'Due date must be before priority date';
  }

}

export interface DialogData {
  task: Task;
}

export interface Project {
  value: string;
  viewValue: string;
}
