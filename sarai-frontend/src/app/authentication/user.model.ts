export interface User {
  id: string;
  displayName: string,
  eMail: string;
  avatarUrl: string;
}
