import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatDividerModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CalendarModule} from './calendar/calendar.module';
import {NavigationComponent} from './navigation/navigation.component';
import {UserNavigationComponent} from './navigation/user-navigation/user-navigation.component';
import {ProjectsComponent} from './projects/projects.component';
import {AuthInterceptor} from './shared/http/auth-interceptor';


@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    NavigationComponent,
    UserNavigationComponent,
    UserNavigationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatMenuModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatSidenavModule,
    CalendarModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
