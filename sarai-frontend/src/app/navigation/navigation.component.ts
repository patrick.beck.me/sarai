import {Component, EventEmitter, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TaskDetailsDialogComponent} from '../calendar/calendar-card/task-details-dialog/task-details-dialog.component';
import {TaskService} from '../calendar/task.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  @Output() openSideNav = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private taskService: TaskService) {
  }

  openNewTaskDialog() {
    const dialogRef = this.dialog.open(TaskDetailsDialogComponent, {
      data: {
        task: {}
      }
    });
    dialogRef.afterClosed().subscribe(task => {
      if (task) {
        this.taskService.createNewTask(task);
      }
    });
  }
}
