import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';

import {TaskService} from './task.service';

describe('TaskService', () => {
  let taskService: TaskService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    taskService = TestBed.get(TaskService);
  });

  it('should be created', () => {
    expect(taskService).toBeTruthy();
  });
});
