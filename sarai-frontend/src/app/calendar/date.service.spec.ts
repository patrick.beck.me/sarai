import {TestBed} from '@angular/core/testing';

import {DateService} from './date.service';

describe('DateService', () => {
  let dateService: DateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dateService = TestBed.get(DateService);
  });

  it('should be created', () => {
    expect(dateService).toBeTruthy();
  });

  it('should return today plus 6 days', () => {
    // given
    const today = new Date();
    const todayPlusSIx = new Date(today);
    todayPlusSIx.setDate(today.getDate() + 6);

    // when
    dateService.getInitialDates().subscribe((days) => {
      // then
      expect(days[0].getDate()).toEqual(today.getDate());
      expect(days[0].getMonth()).toEqual(today.getMonth());
      expect(days[0].getFullYear()).toEqual(today.getFullYear());

      expect(days[6].getDate()).toEqual(todayPlusSIx.getDate());
      expect(days[6].getMonth()).toEqual(todayPlusSIx.getMonth());
      expect(days[6].getFullYear()).toEqual(todayPlusSIx.getFullYear());
    });
  });

  it('should return start date plus 6 days', () => {
    // given
    const startDate = new Date(1561370330556);
    const startDatePlusSix = new Date(startDate);
    startDatePlusSix.setDate(startDate.getDate() + 6);
    // when
    dateService.getNextDates(startDate).subscribe((days) => {
      expect(days[0].getDate()).toEqual(startDate.getDate());
      expect(days[0].getMonth()).toEqual(startDate.getMonth());
      expect(days[0].getFullYear()).toEqual(startDate.getFullYear());

      expect(days[6].getDate()).toEqual(startDatePlusSix.getDate());
      expect(days[6].getMonth()).toEqual(startDatePlusSix.getMonth());
      expect(days[6].getFullYear()).toEqual(startDatePlusSix.getFullYear());
    });
  });

  it('should return true if date is today', () => {
    // given
    const date = new Date();

    // when
    const result = dateService.isDateToday(date);

    // then
    expect(result).toBeTruthy();
  });

  it('should return false if date is not today', () => {
    // given
    const date = new Date();
    date.setDate(date.getDate() + 6);

    // when
    const result = dateService.isDateToday(date);

    // then
    expect(result).toBeFalsy();
  });

  it('should return true if date is tomorrow', () => {
    // given
    const date = new Date();
    date.setDate(date.getDate() + 1);

    // when
    const result = dateService.isDateTomorrow(date);

    // then
    expect(result).toBeTruthy();
  });

  it('should return false if date is not tomorrow', () => {
    // given
    const date = new Date();
    date.setDate(date.getDate() + 9);

    // when
    const result = dateService.isDateTomorrow(date);

    // then
    expect(result).toBeFalsy();
  });

  it('should correct the date time to 00:00:00.000', () => {
    // given
    const date = new Date('2019-06-24T09:58:50.556Z');

    // when
    DateService.correctDateTime(date).subscribe((correctedDate) => {

      // then
      expect(correctedDate.getDate()).toEqual(24);
      expect(correctedDate.getMonth()).toEqual(5);
      expect(correctedDate.getFullYear()).toEqual(2019);
      expect(correctedDate.getHours()).toEqual(0);
      expect(correctedDate.getMinutes()).toEqual(0);
      expect(correctedDate.getSeconds()).toEqual(0);
      expect(correctedDate.getMilliseconds()).toEqual(0);
    });
  });

  it('should keep the date when correcting it', () => {
    // given
    const date = new Date('2019-06-24T00:00:00.000Z');

    // when
    DateService.correctDateTime(date).subscribe((correctedDate) => {

      // then
      expect(correctedDate.getDate()).toEqual(24);
      expect(correctedDate.getMonth()).toEqual(5);
      expect(correctedDate.getFullYear()).toEqual(2019);
    });
  });

});
