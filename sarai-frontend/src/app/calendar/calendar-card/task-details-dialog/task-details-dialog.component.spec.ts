import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {of} from 'rxjs';
import {DateService} from '../../date.service';

import {DialogData, TaskDetailsDialogComponent} from './task-details-dialog.component';

describe('TaskDetailsDialogComponent', () => {
  let component: TaskDetailsDialogComponent;
  let fixture: ComponentFixture<TaskDetailsDialogComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<TaskDetailsDialogComponent>>;

  const data: DialogData = {
    task: {
      id: undefined,
      title: '',
      description: '',
      dueDate: new Date(),
      priorityDate: new Date(),
      isDueTomorrow: false,
      isDueToday: false,
      project: ''
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskDetailsDialogComponent],
      providers: [TaskDetailsDialogComponent, {
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: data
      }, {
        provide: ChangeDetectorRef,
        useValue: jasmine.createSpyObj('ChangeDetectorRef', ['detectChanges'])
      }]
    }).overrideTemplate(TaskDetailsDialogComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    dialogRef = TestBed.get(MatDialogRef);

    fixture = TestBed.createComponent(TaskDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should close the dialog and reset the form', () => {
    // given
    spyOn(component.taskUpdateForm, 'reset');
    // when
    component.onCancel();

    // then
    expect(dialogRef.close).toHaveBeenCalled();
    expect(component.taskUpdateForm.reset).toHaveBeenCalledWith(component.data.task);
  });

  it('should set the corrected priority date to form', () => {
    // given
    const correctedDate = new Date('2019-06-29T00:00:00.000Z');
    spyOn(DateService, 'correctDateTime').and.returnValue(of(correctedDate));

    // when
    component.onSubmit();

    // then
    expect(component.taskUpdateForm.value.priorityDate).toEqual(correctedDate);
  });

  it('should set the corrected due date if present', () => {
    // given
    const correctedDate = new Date('2019-06-29T00:00:00.000Z');
    spyOn(DateService, 'correctDateTime').and.returnValue(of(correctedDate));
    component.taskUpdateForm.value.dueDate = new Date('2019-06-29T05:21:22.333Z');

    // when
    component.onSubmit();

    // then
    expect(component.taskUpdateForm.value.dueDate).toEqual(correctedDate);
  });

  it('should close dialog with form value', () => {
    // given
    spyOn(DateService, 'correctDateTime').and.returnValue(of(new Date()));
    // when
    component.onSubmit();

    // then
    expect(dialogRef.close).toHaveBeenCalledWith(component.taskUpdateForm.value);
  });
});
