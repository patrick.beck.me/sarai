export interface TaskHttpResource {
  id: string;
  userId: string;
  title: string;
  description: string;
  dueDate: string;
  priorityDate: string;
  project: string;
}
