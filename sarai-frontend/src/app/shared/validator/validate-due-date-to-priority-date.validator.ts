import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';
import * as moment from 'moment';

export const dueDateToPriorityDateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const dueDate: Date = control.get('dueDate').value;
  const priorityDate: Date = control.get('priorityDate').value;
  const forbidden = moment(dueDate).isBefore(priorityDate, 'day');
  return forbidden ? {'forbiddenDate': true} : null;
};
