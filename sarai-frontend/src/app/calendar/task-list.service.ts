import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Observable, zip} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AuthenticationService} from '../authentication/authentication.service';
import {User} from '../authentication/user.model';
import {TaskService} from './task.service';
import {TaskList} from './taskList.model';

@Injectable({
  providedIn: 'root'
})
export class TaskListService {

  constructor(private http: HttpClient,
              private taskService: TaskService,
              private authService: AuthenticationService) {
  }

  getTaskListsByPriorityDates(dates: Date[]): Observable<TaskList[]> {
    let currentUser: User = this.authService.getCurrentUser();
    return this.http.get<TaskList[]>(environment.backendUrl + 'task/list', {
      params: {
        startDate: dates[0].toISOString(),
        endDate: dates[6].toISOString(),
        userId: currentUser? currentUser.id : ''
      }
    })
      .pipe(map((tasks: any[]) => {
        let list: TaskList[] = this.initTaskList(dates, []);
        if (tasks && !_.isEmpty(tasks)) {
          list = this.pushTasksIntoList(tasks, list);
        }
        return list;
      }));
  }

  private initTaskList(dates: Date[], list: TaskList[]): TaskList[] {
    dates.forEach(date => {
      list.push({date: date, list: []});
    });
    return list;
  }

  private pushTasksIntoList(tasks: any[], list: TaskList[]): TaskList[] {
    tasks.forEach(task => {
      task.priorityDate = new Date(task.priorityDate);
      task.dueDate = task.dueDate !== '' ? new Date(task.dueDate) : undefined;
      const index = list.findIndex(element => element.date.toISOString() === task.priorityDate.toISOString());
      zip(
        this.taskService.isTaskDueToday(task),
        this.taskService.isTaskDueTomorrow(task)
      ).subscribe((results) => {
        task.isDueToday = results[0];
        task.isDueTomorrow = results[1];
        list[index].list.push(task);
      });
    });
    return list;
  }
}
